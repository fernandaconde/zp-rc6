***Settings***
Documentation       Cadastro de Clientes

Resource            ../resources/base.robot

Test Setup          Login Session
Test Teardown       Finish Session

Test Template       Tentativa de Cadastro de Clientes

***Keywords***
Tentativa de Cadastro de Clientes
    [Arguments]     ${nome}     ${cpf}      ${address}      ${phone_number}     ${message}
    Dado que acesso o formulário de Cadastro de Clientes
    Quando faço a inclusão desse Cliente:
    ...     ${nome}       ${cpf}     ${address}     ${phone_number}
    Então devo ver a mensagem:      ${message}

***Test Cases***
Nome Obrigatório        ${EMPTY}        000.000.029-10      Rua dos Testes, 33      1199999999      Nome é obrigatório
CPF Obrigatório         Fernanda        ${EMPTY}            Rua dos Testes, 33      1199999999      CPF é obrigatório
Endereço Obrigatório    Fernanda        000.000.029-10      ${EMPTY}                1199999999      Endereço é obrigatório
Telefone Obrigatório    Fernanda        000.000.029-10      Rua dos Testes, 33      ${EMPTY}        Telefone é obrigatório